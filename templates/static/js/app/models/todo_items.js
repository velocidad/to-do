/**
 * Created by velocidad on 29/10/13.
 */
var app = app || {};

// to do item model
app.Todo = Backbone.Model.extend({
    urlRoot: "/api/todos/",
    defaults: {
        name: '',
        description: '',
        done: false,
        priority: 0
    },
    //this is going to change the to do status
    toggle: function(){
        this.save({
            done: !this.get('done')
        });
    }
});