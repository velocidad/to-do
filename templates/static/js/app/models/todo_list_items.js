var TodoListItem = Backbone.Model.extend({
    urlRoot: "/api/todos-list/",
    defaults: {
        name: "new TODO list",
        description: "",
        completed: 0,
        overdue: 0
    }
});

//new model instance
//var todoListItem = new TodoListItem({name: 'list name', description: "List desc"});
//todoListList.add(todoListItem);
