var TodoListApp = Backbone.Router.extend({
    routes: {
        "main/": "index",
        "*path": "notFound"
    },
    notFound: function(path){
        alert("This isn't the page you are looking for.");
    },
    start: function(){
        Backbone.history.start({pushState: true});
    },
    index: function(){
        //this.todoListList.fetch();
    },
    show_list: function(list_id){
        console.log("show todo list");
        //this.todoListList.focusOnTodoListItem(list_id);
    },
    initialize: function(data){
        this.todoListList = new TodoListList(data);
        this.todoListListView = new TodoListListView({collection: this.todoListList});
        this.todoListListView.render();
        $('#todolists').html(this.todoListListView.el);
        this.route(/^todos_list\/(\d+)\/$/, 'show_list');
    },
    add_list: function(name, description){
      var newtodoListItem = new TodoListItem();
      var attrs = {description:description, name:name};
      var todoListList = this.todoListList;
      newtodoListItem.save(
          attrs,
          {
            success: function(saved_todo_list, data){
              todoListList.add(saved_todo_list);
            },
            error: function(aborted_todo_list, response){
              console.warn(response);
            }
          }
      );
    },
    edit_list: function(todoListItemId, name, description){
      var todoListList = this.todoListList;
      todoListList.find(function(listItem){
        if(listItem.get('id') == todoListItemId){
          listItem.set({name:name, description:description});
          listItem.save();
          return true;
        }
      });

      var newtodoListItem = new TodoListItem();
      var attrs = {description:description, name:name};
      var todoListList = this.todoListList;
      newtodoListItem.save(
          attrs,
          {
            success: function(saved_todo_list, data){
              todoListList.add(saved_todo_list);
            },
            error: function(aborted_todo_list, response){
              console.warn(response);
            }
          }
      );
    },
    delete_list: function(todoListItemId){
      var todoListList = this.todoListList;
      todoListList.find(function(listItem){
        if(listItem.get('id') == todoListItemId){
          console.log(listItem);
          todoListList.remove(listItem);
          listItem.destroy();
          return true;
        }
      });
    }
});


/*var ListsApp = new (Backbone.View.extend({
    Collections: {
        todoListList: new TodoListList()
    },
    Views: {
        todoListList: new TodoListListView({collection: ListsApp.Collections.todoListList})
    },
    Routes: {
        todoListRouter: TodoListApp()
    },
    events: {
        "click a": "selectTaskList"
    },
    selectTaskList: function(e){
        var navigate_to = "todos_list/" + e.target.pathname.split("#")[1] + "/";
        e.preventDefault();
        $('.selected').removeClass('selected');
        $(this).addClass('selected');
        ListsApp.Routes.todoListRouter.navigate(navigate_to, {trigger: true});
    },
    start: function(bootstrap){
        var todosList = new ListsApp.Collections.todoListList(bootstrap);
        var todosListView = new ListsApp.Views.todoListList({collection: todosList})
        todosListView.render();
        $('#todolists').html(this.todoListListView.el);
        Backbone.history.start({pushState: true});
    }
}))({el: $('ul .nav nav-list')});*/