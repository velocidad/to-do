var TodoListList = Backbone.Collection.extend({
    model: TodoListItem,
    url: "/api/todos-list/"
});