/**
 * Created by velocidad on 29/10/13.
 */
var app = app || {};

//To do's collection
var TodoList = Backbone.Collection.extend({
    url: "/api/todos_list/1", //TODO change list id dinamically
    model: app.Todo,
    // filter the completed todos
    completed: function(){
        return this.filter(function(todo){
            return todo.get('done');
        });
    },
    //items to be completed
    remaining: function(){
        return this.without.apply(this, this.completed());
    },
    //generates the next 'order' number
    nextOrder: function(){
        if(!this.length){
            return 1;
        }
        return this.last().get('order') + 1;
    },
    comparator: function(todo){
        return todo.get('order');
    }
});

app.Todos = new TodoList();
