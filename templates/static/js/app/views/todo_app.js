/**
 * Created by velocidad on 29/10/13.
 */
var app = app || {};

app.AppView = Backbone.View.extend({
    // bind the existing HTML
    el: '#todoapp',
    //template for the to do list stats
    statsTemplate: _.template($("#stats-template").html()),
    //delegating events for creating new items, and clearing completed ones
    events: {
        'keypress #new-todo': 'createOnEnter',
        'click #clear-completed': 'clearCompleted',
        'click #toggle-all': 'toggleAllComplete'
    },
    //bind the relevant events(add, changed) on the to dos collection
    initialize: function(){
        this.allCheckbox = this.$('#toggle-all')[0];
        this.$input = this.$('#new-todo');
        this.$footer = this.$('#footer');
        this.$main = this.$('#main');

        this.listenTo(app.Todos, 'add', this.addOne);
        this.listenTo(app.Todos, 'reset', this.addAll);

        this.listenTo(app.Todos, 'change:completed', this.filterOne);
        this.listenTo(app.Todos, 'filter', this.filterAll);
        this.listenTo(app.Todos, 'all', this.render);

        app.Todos.fetch();
    },

    //add a single todoItem to the list
    addOne: function(todo){
        var view = new app.TodoView({model: todo});
        $('#todo-list').append(view.render().el);
    },
    //add all items in the to do's collection
    addAll: function(){
        this.$('#todo-list').html('');
        app.Todos.each(this.addOne, this);
    },
    //re-render the app, refresh statistics
    render: function(){
        var completed = app.Todos.completed().length;
        var remaining = app.Todos.remaining().length;

        if(app.Todos.length){
            this.$main.show();
            this.$footer.show();

            this.$footer.html(this.statsTemplate({
                completed: completed,
                remaining: remaining}));

            this.$('#filters').find('li').find('a')
                .removeClass('selected')
                .filter('[href="#/"' + (app.TodoFilter || '') + ']')
                .addClass('selected');
        }else{
            this.$main.hide();
            this.$footer.hide();
        }

        this.allCheckbox.checked = !remaining;

    },

    filterOne: function(todo){
        todo.trigger('visible');
    },

    //add all items in the "to do's" collection at once
    filterAll: function(){
        app.Todos.each(this.filterOne, this);
    },

    //generate the attributes for a new item
    newAttributes: function(){
        return {
            name: this.$input.val().trim(),
            order: app.Todos.nextOrder(),
            completed: false,
            description: '',
            priority: 0,
            todo_list: 1 //TODO change to do list dinamicaly
        }
    },
    //create new item on press enter in the new to do field
    createOnEnter: function(event){
        if(event.which !== ENTER_KEY || !this.$input.val().trim()){
            return;
        }
        app.Todos.create(this.newAttributes());
        this.$input.val('');
    },

    //clear all the completed to dos, destroying their models
    clearCompleted: function(){
        _.invoke(app.Todos.completed(), 'destroy');
        return false;
    },
    toggleAllComplete: function(){
        var completed = this.allCheckbox.checked;

        app.Todos.each(function(todo){
            todo.save({
                'completed': completed
            });
        });
    }



});