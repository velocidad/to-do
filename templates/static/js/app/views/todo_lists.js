var TodoListView = Backbone.View.extend({
    tagName: 'li',
    template: _.template(
        '<div class="list_progress">' +
        '<span class="green" width="<%= completed %>%"></span>' +
        '<span class="red" width="<%= overdue %>%"></span>' +
        '</div>' +
        '<a href="#<%= id %>" id="#<%= id %>"><%= name %></a>'),
    initialize: function(){
        this.model.on('change', this.render, this);
        this.model.on('destroy', this.remove, this);
    },
    render: function(){
        var attributes = this.model.toJSON();
        this.$el.html(this.template(attributes));
    },
    remove: function(){
        this.$el.remove();
    }
});


var TodoListListView = Backbone.View.extend({
    tagName: 'ul',
    className: 'nav nav-list',
    initialize: function(){
        this.collection.on('add', this.addOne, this);
        this.collection.on('reset', this.addAll, this);
    },
    render: function(){
        this.addAll();
    },
    addOne: function(todoListItem){
        var todoListView = new TodoListView({model: todoListItem});
        todoListView.render();
        this.$el.append(todoListView.el);
    },
    addAll: function(){
        this.$el.html("");
        this.collection.forEach(this.addOne, this);
        this.$el.prepend(
                $('<li>').append($('<a>')
                    .attr({'href': '#add_list', 'id': 'addList'})
                    .html('<span class="icon-plus"></span>Nueva lista')))
            .prepend($('<li>').text("Task Lists").addClass('nav-header'));
    }
});