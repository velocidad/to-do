/**
 * Created by velocidad on 30/10/13.
 */
var app = app || {};

//to do item view
//the DOM element for a to do item
app.TodoView = Backbone.View.extend({
    //generates a li element
    tagName: 'li',
    //template for a single item
    template: _.template($('#item-template').html()),
    events: {
        'dblclick label': 'edit',
        'keypress .edit': 'updateOnEnter',
        'blur .edit': 'close'
    },

    //The TodoView listens for changes to its model, re-rendering. Since there's
    //a one-to-one correspondence between a **To do** and a **TodoView** in this
    //app, we set a direct reference on the model for convenience.
    initialize: function(){
        this.listenTo(this.model, 'change', this.render);
    },
    // re-render the titles of the to do item
    render: function(){
        this.$el.html(this.template(this.model.toJSON()));
        this.$input = this.$('input.edit');
        this.$description = this.$('textarea.edit');
        return this;
    },
    //switch the view into "edit mode"
    edit: function(){
        this.$el.addClass('editing');
        this.$input = this.focus();
    },
    close: function(){
        var value = this.$input.val().trim();
        var value_desc = this.$description.val().trim();
        if(value){
            this.model.save({name:value,
                description:value_desc});
        }
        this.$el.removeClass('editing');
    },
    //if you press ENTER we are done editing
    updateOnEnter: function(e){
        if(e.which === ENTER_KEY){
            this.close();
        }
    }

});