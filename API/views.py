#REST Framework
from rest_framework import generics

from rest_framework.permissions import IsAuthenticated

from django.contrib.auth.models import User
from todo_items.models import TodoItem, TodoList

from API.serializers import TodoItemSerializer, TodoListSerializer, \
    UserSerializer
from API.permissions import IsTodoOwnerOrReadOnly, IsTodoListOwnerOrNone


class TodoItemList(generics.ListCreateAPIView):
    """
    List all todo items, or create a new todo.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = TodoItemSerializer

    def get_queryset(self):
        return TodoItem.objects.filter(todo_list__owner=self.request.user)


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a code tdo_item.
    """
    permission_classes = (IsAuthenticated, IsTodoOwnerOrReadOnly)
    queryset = TodoItem.objects.all()
    serializer_class = TodoItemSerializer


class ListOfTodosDetail(generics.ListCreateAPIView):
    """
    List all todo items, or create a new todo.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = TodoItemSerializer

    def get_queryset(self):
        return TodoItem.objects.filter(
            todo_list__owner=self.request.user,
            todo_list__pk=self.kwargs['list_pk'])


class TodoListList(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = TodoListSerializer

    def get_queryset(self):
        return TodoList.objects.filter(owner=self.request.user)

    def pre_save(self, obj):
        obj.owner = self.request.user


class TodoListDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (IsTodoListOwnerOrNone,)
    queryset = TodoList.objects.all()
    serializer_class = TodoListSerializer

    def pre_save(self, obj):
        obj.owner = self.request.user


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def pre_save(self, obj):
        obj.owner = self.request.user


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def pre_save(self, obj):
        obj.owner = self.request.user