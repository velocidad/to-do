from django.conf.urls import patterns, url, include
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, routers
from rest_framework.urlpatterns import format_suffix_patterns

from API import views
#Routers provide an easy way of automatucally determining the url conf

urlpatterns = patterns(
    '',
    #API
    #All the to dos
    url(r'^todos/$', views.TodoItemList.as_view(), name='todo_list'),
    #Detail of one item
    url(r'^todos/(?P<pk>[0-9]+)$', views.TodoDetail.as_view(),
        name="todo_detail"),
    #Items by list id
    url(r'^todos_list/(?P<list_pk>[0-9]+)$', views.ListOfTodosDetail.as_view(),
        name="list_of_todos_detail"),
    #all the lists for the logged
    url(r'^todos-list/$', views.TodoListList.as_view(), name='todo_list_list'),
    #detail of one list
    url(r'^todos-list/(?P<pk>[0-9]+)$', views.TodoListDetail.as_view(),
        name="todo_list_detail"),
    #Show all users and owned to dos items
    url(r'^users/$', views.UserList.as_view(), name='user_list'),
    #Show one user and his owned to dos items
    url(r'^users/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(),
        name="todo_list_detail"),

)
urlpatterns = format_suffix_patterns(urlpatterns)

urlpatterns += patterns('',
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
)