from rest_framework import permissions

from todo_items.models import TodoList, TodoItem

class IsTodoOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow the owner to modify a to do
    """
    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True
        # Write permissions are only allowed  to the owner off the to do

        return obj.todo_list.owner == request.user


class IsTodoListOwnerOrNone(permissions.BasePermission):
    """
    Custom permission to only allow the owner to see and modify a to do
    """
    def has_object_permission(self, request, view, obj):
        # Permissions are only allowed  to the owner off the to do
        return obj.owner == request.user