#from django.forms import widgets
from rest_framework import serializers
from django.contrib.auth.models import User
from todo_items.models import TodoItem, TodoList

class UserSerializer(serializers.ModelSerializer):
    todo_lists = serializers.PrimaryKeyRelatedField(many=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'owner')


class TodoListSerializer(serializers.ModelSerializer):
    completed = serializers.Field(source='completed_tasks')
    overdue = serializers.Field(source='overdue_tasks')
    owner = serializers.Field(source='owner.username')

    class Meta:
        model = TodoList
        fields = ('id', 'name', 'description', 'owner', 'completed', 'overdue')


class TodoItemSerializer(serializers.ModelSerializer):
    def get_fields(self, *args, **kwargs):
        fields = super(TodoItemSerializer, self).get_fields(*args, **kwargs)
        fields['todo_list'].queryset = fields['todo_list'].queryset.filter(
            owner=self.context['view'].request.user)
        return fields

    class Meta:
        model = TodoItem
    # Custom serializer
    #pk = serializers.Field()
    #name = serializers.CharField(required=True, max_length=140)
    #todo_list = serializers.CharField(required=True, max_length=140)
    #description = serializers.CharField(required=False, max_length=10000)
    #due_date = serializers.DateTimeField(required=False)
    #priority = serializers.ChoiceField(choices=PRIORITY, default=1)
    #done = serializers.BooleanField(required=True, default=False)
    #
    #def restore_object(self, attrs, instance=None):
    #    """
    #    Create or update a TodoItem, given a dictionary of deserialized field
    #    values.
    #    If we don't define this method, deserializing data will only
    #    return a dict of items
    #    """
    #    if instance:
    #        #update instance
    #        instance.name = attrs.get('title', instance.name)
    #        instance.todo_list = attrs.get('todo_list', instance.todo_list)
    #        instance.description = attrs.get('description',
    #                                         instance.description)
    #        instance.due_date = attrs.get('due_date', instance.due_date)
    #        instance.priority = attrs.get('priority', instance.priority)
    #        instance.done = attrs.get('done', instance.done)
    #
    #    return TodoItem(**attrs)