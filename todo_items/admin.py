from django.contrib import admin
from todo_items.models import TodoItem, TodoList

admin.site.register(TodoItem)
admin.site.register(TodoList)
