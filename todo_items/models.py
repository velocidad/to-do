import datetime

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

PRIORITY = ((0, "Low"), (1, "Normal"), (2, "Urgent"))


class ElementQuerySet(models.query.QuerySet):
    def done(self):
        return self.filter(done=True)

    def incomplete(self):
        return self.filter(done=False)

    def overdue(self):
        _now = datetime.datetime.now()
        current_timezone = timezone.get_current_timezone()
        _now = current_timezone.localize(_now)
        return self.filter(due_date__lte=_now)

    def upcoming(self):
        upcoming_date = datetime.datetime.now() + datetime.timedelta(days=1)
        return self.filter(due_date__lte=upcoming_date)


class ElementManager(models.Manager):
    def get_query_set(self):
        return ElementQuerySet(self.model, using=self._db)

    def done(self):
        return self.get_query_set().done()

    def incomplete(self):
        return self.get_query_set().incomplete()

    def overdue(self):
        return self.get_query_set().overdue()

    def upcoming(self):
        return self.get_query_set().upcoming()


class TodoList(models.Model):
    name = models.CharField(max_length=140)
    description = models.TextField(null=True, blank=True)
    owner = models.ForeignKey(User, on_delete=models.PROTECT,
                              related_name='owner')

    def __unicode__(self):
        return self.name

    @property
    def completed_tasks(self):
        todos = self.todos.all()
        total = len(todos)
        if total:
            completed = 0
            for todo in todos:
                if todo.done:
                    completed += 1
            return int(100*float(completed)/total)
        return 0

    @property
    def overdue_tasks(self):
        todos = self.todos.all()
        total = len(todos)
        _now = datetime.datetime.now()
        current_timezone = timezone.get_current_timezone()
        _now = current_timezone.localize(_now)
        if total:
            overdue = 0
            for todo in todos:
                if not todo.done and todo.due_date:
                    if _now > todo.due_date:
                        overdue += 1
            return int(100*float(overdue)/total)
        return 0


class TodoItem(models.Model):
    todo_list = models.ForeignKey(TodoList, related_name='todos')
    name = models.CharField(max_length=140)
    description = models.TextField(null=True, blank=True)
    due_date = models.DateTimeField(null=True, blank=True)
    priority = models.IntegerField(choices=PRIORITY, default=1)
    asigned_to = models.ForeignKey(User, on_delete=models.PROTECT,
                                   null=True, blank=True)
    done = models.BooleanField(default=False)
    order = models.SmallIntegerField(default=0)
    reminder = models.BooleanField(default=False)

    objects = ElementManager()

    def __unicode__(self):
        return " - ".join((self.todo_list.name, self.name, str(self.due_date)))