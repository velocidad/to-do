import json

from django.shortcuts import render_to_response, HttpResponseRedirect
from django.template.context import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm

from todo_items.models import TodoList

def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/main/")
    form = AuthenticationForm(data=request.POST or None)
    username = ''
    password = ''
    error = ''
    if form.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                url = "/main/"
                try:
                    ur_get = request.META['HTTP_REFERER']
                except KeyError:
                    pass
                else:
                    ur_get = ur_get.split("next=")
                    if len(ur_get) > 1:
                        url = ur_get[1]
                return HttpResponseRedirect(url)
            else:
                error = "Tu cuenta ha sido desactivada, por favor ponte en " \
                        "contacto con tu administrador"
        else:
            error = "Tu nombre de usuario o contrase&ntilde;a son incorrectos."
    variables = dict(username=username, password=password, error=error,
                     form=form)
    variables_template = RequestContext(request, variables)
    return render_to_response("login.html", variables_template)


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/main/?logout')


@login_required(login_url="/")
def main(request):

    todos_list = TodoList.objects.filter(owner=request.user)
    lists = []
    for _list in todos_list:
        lists.append(dict(id=_list.id,
                          name=_list.name,
                          description=_list.description,
                          owner=_list.owner.username,
                          completed=_list.completed_tasks,
                          overdue=_list.overdue_tasks))
    variables = dict(lista=json.dumps(lists))
    variables_template = RequestContext(request, variables)
    return render_to_response("index.html", variables_template)


@login_required(login_url="/")
def manage(request):
    variables = dict()
    variables_template = RequestContext(request, variables)
    return render_to_response("index.html", variables_template)
